import csv
import os
from dataclasses import dataclass
from pathlib import Path
from typing import Generator

import psycopg
from pyproj import Transformer
from tqdm import tqdm

transformer = Transformer.from_crs(2154, 4326)


def lambert_to_latlon(x: float, y: float) -> tuple[float, float]:
    return transformer.transform(x, y)


def creation_table_communes(cur: psycopg.cursor) -> None:
    sql = """
        DROP VIEW IF EXISTS pharmacies;
        DROP VIEW IF EXISTS densite_medicale;
        DROP TABLE IF EXISTS liste_communes;
        CREATE TABLE liste_communes (
            code_commune_insee VARCHAR,
            nom_de_la_commune VARCHAR,
            code_postal VARCHAR,
            ligne_5 VARCHAR,
            libelle_d_acheminement VARCHAR,
            lat FLOAT,
            lon FLOAT
        )
    """
    cur.execute(sql)


def creation_table_equipements(cur: psycopg.cursor) -> None:
    sql = """
        DROP VIEW IF EXISTS pharmacies;
        DROP VIEW IF EXISTS densite_medicale;
        DROP TABLE IF EXISTS liste_equipements;
        CREATE TABLE liste_equipements (
            AAV2020 VARCHAR,
            AN VARCHAR,
            BV2012 VARCHAR,
            DCIRIS VARCHAR,
            DEP VARCHAR,
            DEPCOM VARCHAR,
            EPCI INT,
            LABEL VARCHAR,
            LAT FLOAT,
            LON FLOAT,
            QUALITE_XY VARCHAR,
            REG VARCHAR,
            TYPEQU VARCHAR,
            UU2020 VARCHAR
        )

    """
    cur.execute(sql)


def create_table_departements(cur: psycopg.cursor) -> None:
    sql = """
        DROP VIEW IF EXISTS densite_medicale;
        DROP TABLE IF EXISTS donnees_departement;
        CREATE TABLE donnees_departement (
            CODREG VARCHAR,
            REG VARCHAR,
            CODDEP VARCHAR,
            DEP VARCHAR,
            NBARR INT,
            NBCAN VARCHAR,
            NBCOM INT,
            PMUN INT,
            PTOT INT
        )
    """
    cur.execute(sql)


def get_rows_departements() -> Generator[dict[str, str | float], None, None]:
    with Path(__file__).parent.joinpath("donnees_departements.csv").open("r") as f:
        reader = csv.DictReader(f, delimiter=";")
        for i, row in enumerate(reader):
            yield {
                "CODREG": str(row["CODREG"]),
                "REG": str(row["REG"]),
                "CODDEP": str(row["CODDEP"]),
                "DEP": str(row["DEP"]),
                "NBARR": int(row["NBARR"]),
                "NBCAN": str(row["NBCAN"]),
                "NBCOM": int(row["NBCOM"]),
                "PMUN": int(row["PMUN"]),
                "PTOT": int(row["PTOT"]),
            }


def get_rows_equipements() -> Generator[dict[str, str | float], None, None]:
    with Path(__file__).parent.joinpath("bpe20_ensemble_xy.csv").open("r") as f:
        reader = csv.DictReader(f, delimiter=";")
        for i, row in enumerate(reader):
            if (row["LAMBERT_X"] != "") & (row["LAMBERT_X"] != ""):
                lat, lon = lambert_to_latlon(
                    float(row["LAMBERT_X"]), float(row["LAMBERT_Y"])
                )
            else:
                lat, lon = 0, 0
            yield {
                "AAV2020": str(row["AAV2020"]),
                "AN": str(row["AN"]),
                "BV2012": str(row["BV2012"]),
                "DCIRIS": str(row["DCIRIS"]),
                "DEP": str(row["DEP"]),
                "DEPCOM": str(row["DEPCOM"]),
                "EPCI": 0 if row["EPCI"] == "" else int(row["EPCI"]),
                "LABEL": str(row["LABEL"]),
                "LAT": lat,
                "LON": lon,
                "QUALITE_XY": str(row["QUALITE_XY"]),
                "REG": str(row["REG"]),
                "TYPEQU": str(row["TYPEQU"]),
                "UU2020": 0 if row["UU2020"] == "" else str(row["UU2020"]),
            }


def get_rows_communes() -> Generator[dict[str, str | float], None, None]:
    with Path(__file__).parent.joinpath("3d30becb-f4ee-4541-9e06-7a80bb18fefb").open(
        "r"
    ) as f:
        reader = csv.DictReader(f, delimiter=";")
        for i, row in enumerate(reader):
            yield {
                "code_commune_insee": str(row["code_commune_insee"]),
                "nom_de_la_commune": str(row["nom_de_la_commune"]),
                "code_postal": str(row["code_postal"]),
                "ligne_5": str(row["ligne_5"]),
                "libelle_d_acheminement": str(row["libelle_d_acheminement"]),
                "coordonnees_gps": str(row["coordonnees_gps"]),
            }


if __name__ == "__main__":
    uri = os.getenv("PG_API0057_URI_RW")
    with psycopg.connect(uri, row_factory=psycopg.rows.dict_row) as conn:

        with conn.cursor() as cur:
            creation_table_equipements(cur)
            with cur.copy(
                "COPY liste_equipements (AAV2020, AN, BV2012, DCIRIS, DEP, DEPCOM, EPCI, LABEL, LAT, LON, QUALITE_XY, REG, TYPEQU, UU2020 ) FROM STDIN"
            ) as copy:
                for row in tqdm(
                    get_rows_equipements(), desc="Copying liste_equipements"
                ):
                    copy.write_row(
                        [
                            row["AAV2020"],
                            row["AN"],
                            row["BV2012"],
                            row["DCIRIS"],
                            row["DEP"],
                            row["DEPCOM"],
                            row["EPCI"],
                            row["LABEL"],
                            row["LAT"],
                            row["LON"],
                            row["QUALITE_XY"],
                            row["REG"],
                            row["TYPEQU"],
                            row["UU2020"],
                        ]
                    )
            create_table_departements(cur)
            with cur.copy(
                "COPY donnees_departement (CODREG, REG, CODDEP, DEP, NBARR, NBCAN, NBCOM, PMUN, PTOT) FROM STDIN"
            ) as copy:
                for row in tqdm(
                    get_rows_departements(), desc="Copying donnees_departement"
                ):
                    copy.write_row(
                        [
                            row["CODREG"],
                            row["REG"],
                            row["CODDEP"],
                            row["DEP"],
                            row["NBARR"],
                            row["NBCAN"],
                            row["NBCOM"],
                            row["PMUN"],
                            row["PTOT"],
                        ]
                    )

            conn.commit()

            creation_table_communes(cur)

            with cur.copy(
                "COPY liste_communes (code_commune_insee, nom_de_la_commune, code_postal, ligne_5, libelle_d_acheminement, lat, lon) FROM STDIN"
            ) as copy:
                for row in tqdm(get_rows_communes(), desc="Copying liste_communes"):
                    if row["coordonnees_gps"] != "":
                        lat, lon = row["coordonnees_gps"].split(",")
                    else:
                        lat, lon = 0, 0
                    copy.write_row(
                        [
                            row["code_commune_insee"],
                            row["nom_de_la_commune"],
                            row["code_postal"],
                            row["ligne_5"],
                            row["libelle_d_acheminement"],
                            lat,
                            lon,
                        ]
                    )

            requete_densite_dept = """
                CREATE VIEW densite_medicale AS
                WITH
                    nb_equipement_departement AS (
                        SELECT
                            dep,
                            COUNT(*) as nb_equipements_medicaux
                        FROM
                            liste_equipements
                        WHERE
                            typequ LIKE 'D1%' OR
                            typequ LIKE 'D2%'
                        GROUP BY dep
                    )
                SELECT
                    dd.dep,
                    dd.ptot,
                    ned.nb_equipements_medicaux,
                    CAST(ned.nb_equipements_medicaux AS FLOAT)/dd.ptot as densité
                FROM
                    donnees_departement dd,
                    nb_equipement_departement ned
                WHERE
                    dd.coddep = ned.dep
                ORDER BY densité DESC;
                """

            requete_desert_dept = """
                SELECT
                    SUM(ptot)
                FROM
                    densite_medicale
                WHERE
                    densité < 0.7*(SELECT AVG(densité) FROM densite_medicale)
                """

            ajout_geometries = """
                ALTER TABLE liste_equipements ADD COLUMN emplacement geometry(Point, 4326);
                UPDATE liste_equipements SET emplacement = ST_SetSRID(ST_MakePoint(lon, lat), 4326);
                CREATE INDEX ON liste_equipements(emplacement::geography);
                ALTER TABLE liste_communes ADD COLUMN emplacement geometry(Point, 4326);
                UPDATE liste_communes SET emplacement = ST_SetSRID(ST_MakePoint(lon, lat), 4326);
                CREATE INDEX ON liste_communes(emplacement::geography);
            """

            creer_vue_pharmacies = """
                CREATE VIEW pharmacies AS
                    SELECT
                        *
                    FROM
                        liste_equipements
                    WHERE
                        typequ = 'D307';
                """

            creer_vue_pharmacies = """
                CREATE VIEW pharmacies AS
                    SELECT
                        *
                    FROM
                        liste_equipements
                    WHERE
                        typequ = 'D307';
                """

            find_nearest_pharmacy = """
                SELECT
                    l.code_commune_insee,
                    p.typequ,
                    p.distance as dist
                FROM
                    liste_communes l
                    CROSS JOIN LATERAL (
                        SELECT
                            typequ,
                            emplacement::geography <-> l.emplacement::geography as distance
                        FROM
                            pharmacies
                        ORDER BY distance
                        LIMIT  1
                        ) AS p
                ORDER BY dist DESC
                ;
            """

            cur.execute(requete_densite_dept)
            conn.commit()
            cur.execute(requete_desert_dept)
            print()
            print(
                f'Le nombre de personnes vivant dans un désert médical est : {cur.fetchall()[0]["sum"]}'
            )
            print()
            cur.execute(ajout_geometries)
            conn.commit()
            cur.execute(creer_vue_pharmacies)
            conn.commit()
            cur.execute(find_nearest_pharmacy)
            conn.commit()
